package xyz.z3ro.speechsynthesizer;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.speech.tts.TextToSpeech;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.card.MaterialCardView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.app.ActivityCompat;
import xyz.z3ro.speechsynthesizer.utilities.Constants;
import xyz.z3ro.speechsynthesizer.utilities.PermissionUtil;

public class MainActivity extends AppCompatActivity
        implements TextToSpeech.OnInitListener, View.OnClickListener, AdapterView.OnItemSelectedListener,
        ActivityCompat.OnRequestPermissionsResultCallback {

    //text to speech engine
    private TextToSpeech textToSpeech;

    //views
    private Button buttonSave;
    private FloatingActionButton floatingActionButtonSpeak;
    private AppCompatEditText editText;
    private Spinner spinner;
    private TextView textViewDeviceID;
    private MaterialCardView materialCardView;

    //TAG
    private static final String TAG = "MainActivity";
    private static final int STORAGE_PERMISSION_REQUEST_CODE = 7;
    private static final int PHONE_PERMISSION_REQUEST_CODE = 8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Setting up TTS
        textToSpeech = new TextToSpeech(this, this);

        // Set window resizable on soft keyboard input mode
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        // Initialise shared preferences
//        sharedPreferences = this.getSharedPreferences(Constants.PREFERENCE_FILE_NAME, this.MODE_PRIVATE);

        //Setting up text view
        materialCardView = findViewById(R.id.cardView_container);
        textViewDeviceID = findViewById(R.id.textView_deviceID);
        setDeviceID();

        //Setting up buttons
        buttonSave = findViewById(R.id.materialButton_save);
        buttonSave.setEnabled(false);
        buttonSave.setOnClickListener(this);

        floatingActionButtonSpeak = findViewById(R.id.floatingActionButton_speak);
        floatingActionButtonSpeak.setEnabled(false);
        floatingActionButtonSpeak.setOnClickListener(this);

        //Setting up spinner
        spinner = findViewById(R.id.spinner_languageSelect);
        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(this, R.array.languages_array, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
        spinner.setOnItemSelectedListener(this);


        //Setting up edit text
        editText = findViewById(R.id.editText_input);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0)
                    buttonSave.setEnabled(true);
                else
                    buttonSave.setEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    protected void onDestroy() {
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        super.onDestroy();
    }

    @Override
    public void onInit(int i) {
        if (i == TextToSpeech.SUCCESS) {
            int result = textToSpeech.setLanguage(Locale.US);
            textToSpeech.setSpeechRate(Constants.speechRate);
            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Toast.makeText(MainActivity.this, "Language not supported", Toast.LENGTH_LONG).show();
                Log.e(TAG, "Language not supported");
            } else {
                floatingActionButtonSpeak.setEnabled(true);
                speak();
            }
        } else {
            Toast.makeText(MainActivity.this, "TTS Engine initialisation failed", Toast.LENGTH_LONG).show();
            Log.e(TAG, "Initialization failed");
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.floatingActionButton_speak:
                speak();
                break;
            case R.id.materialButton_save:
                saveSynthesis();
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Log.d(TAG, "Position " + i);
        switch (i) {
            case 0:
                Log.d(TAG, "0 SELECTED");
                textToSpeech.setLanguage(Locale.US);
                break;
            case 1:
                Log.d(TAG, "1 SELECTED");
                textToSpeech.setLanguage(new Locale("hi"));
                break;
            default:
                textToSpeech.setLanguage(Locale.US);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        textToSpeech.setLanguage(Locale.US);
        Log.d(TAG, "NOTHING SELECTED");
    }

    /**
     * Helper function to get Device ID
     */
    private String getDeviceID() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (isPermissionGranted(PHONE_PERMISSION_REQUEST_CODE)) {
                try {
                    return Build.getSerial();
                } catch (SecurityException e) {
                    Log.e(TAG, MainActivity.this.getString(R.string.get_serial_error));
                    e.printStackTrace();
                }
            } else {
                Log.e(TAG, MainActivity.this.getString(R.string.get_serial_error));
                return null;
            }
        } else {
            if (isPermissionGranted(PHONE_PERMISSION_REQUEST_CODE))
                return getDeviceIDOld();
            else {
                Log.e(TAG, MainActivity.this.getString(R.string.get_serial_error));
                return null;
            }
        }
        return null;
    }

    @TargetApi(25)
    private String getDeviceIDOld() {
        return Build.SERIAL;
    }

    /**
     * Helper function to set Device ID to textViews
     */
    private void setDeviceID() {
        String deviceID = this.getString(R.string.textView_deviceID, getDeviceID());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (isPermissionGranted(PHONE_PERMISSION_REQUEST_CODE)) {
                textViewDeviceID.setText(deviceID);
            } else {
                reqPermission(PHONE_PERMISSION_REQUEST_CODE);
            }
        } else {
            textViewDeviceID.setText(deviceID);
        }
    }

    /**
     * Helper function to synthesize speech
     */
    private void speak() {
        String speech = editText.getText().toString();
        textToSpeech.speak(speech, TextToSpeech.QUEUE_FLUSH, null);
    }

    /**
     * Helper function to save speech synthesis to file
     */
    private void saveSynthesis() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (isPermissionGranted(STORAGE_PERMISSION_REQUEST_CODE)) {
                saveFile();
            } else {
                reqPermission(STORAGE_PERMISSION_REQUEST_CODE);
            }
        } else {
            saveFile();
        }
    }

    /**
     * Helper function to save file
     */
    private void saveFile() {
        CharSequence speech = editText.getText().toString();
        String storagePath = Environment.getExternalStorageDirectory().getAbsolutePath();
        File path = new File(storagePath + "/SpeechSynthesizer/");
        if (!path.exists())
            path.mkdirs();
        String fileName = speech.toString();
        fileName += ".wav";
        String destinationFilePath = path.getAbsolutePath() + File.separator + fileName;
        File destinationFile = new File(destinationFilePath);
        Log.d(TAG, destinationFilePath);
        textToSpeech.synthesizeToFile(speech, null, destinationFile, "save");
        Snackbar.make(findViewById(android.R.id.content), "File saved in " + destinationFilePath, Snackbar.LENGTH_SHORT).show();
        File nomedia = new File(path, ".nomedia");
        if (!nomedia.exists()) {
            try {
                nomedia.createNewFile();
            } catch (IOException e) {
                Log.d(TAG, MainActivity.this.getString(R.string.nomedia_write_error));
                e.printStackTrace();
            }
        }
    }

//    /**
//     * Helper function to create nomedia file
//     */
//    private void createNoMedia(final File noMediaPath) {
//        Runnable runnable = new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    noMediaPath.createNewFile();
//                } catch (IOException e) {
//                    Log.d(TAG, MainActivity.this.getString(R.string.nomedia_write_error));
//                    e.printStackTrace();
//                }
//            }
//        };
//        runnable.run();
//    }

    /**
     * Helper function to check if READ/WRITE external storage permission is granted
     */
    private boolean isPermissionGranted(int requestCode) {
        switch (requestCode) {
            case STORAGE_PERMISSION_REQUEST_CODE:
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    return true;
                } else
                    return false;
            case PHONE_PERMISSION_REQUEST_CODE:
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED)
                    return true;
                else
                    return false;
            default:
                return false;
        }
    }

    /**
     * Helper function to request storage permission
     */
    private void reqPermission(int requestCode) {
        switch (requestCode) {
            case STORAGE_PERMISSION_REQUEST_CODE:
                String[] permissionsStorage = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
                ActivityCompat.requestPermissions(MainActivity.this, permissionsStorage, STORAGE_PERMISSION_REQUEST_CODE);
                break;
            case PHONE_PERMISSION_REQUEST_CODE:
                String[] permissionsPhone = new String[]{Manifest.permission.READ_PHONE_STATE};
                ActivityCompat.requestPermissions(MainActivity.this, permissionsPhone, PHONE_PERMISSION_REQUEST_CODE);

        }
    }

    /**
     * Callback for the result from requesting permissions. This method
     * is invoked for every call on {@link #requestPermissions(String[], int)}.
     * <p>
     * <strong>Note:</strong> It is possible that the permissions request interaction
     * with the user is interrupted. In this case you will receive empty permissions
     * and results arrays which should be treated as a cancellation.
     * </p>
     *
     * @param requestCode  The request code passed in {@link #requestPermissions(String[], int)}.
     * @param permissions  The requested permissions. Never null.
     * @param grantResults The grant results for the corresponding permissions
     *                     which is either {@link PackageManager#PERMISSION_GRANTED}
     *                     or {@link PackageManager#PERMISSION_DENIED}. Never null.
     * @see #requestPermissions(String[], int)
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case STORAGE_PERMISSION_REQUEST_CODE:
                if (PermissionUtil.verifyPermissions(grantResults)) {
                    saveFile();
                } else {
                    Snackbar.make(findViewById(android.R.id.content), R.string.storage_permissions_denied, Snackbar.LENGTH_LONG).show();
                }
                break;
            case PHONE_PERMISSION_REQUEST_CODE:
                if (PermissionUtil.verifyPermissions(grantResults)) {
                    setDeviceID();
                } else {
                    materialCardView.setVisibility(View.GONE);
                    textViewDeviceID.setVisibility(View.GONE);
                    Snackbar.make(findViewById(android.R.id.content), R.string.phone_permissions_denied, Snackbar.LENGTH_LONG).show();
                }
        }
    }
}
