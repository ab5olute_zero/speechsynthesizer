package xyz.z3ro.speechsynthesizer.utilities;

public class Constants {
    public static final String PREFERENCE_FILE_NAME = "speechsynthesizer_preferences";
    public static final String PREFERENCE_FIRST_RUN = "firstrun";

    public static final float speechRate = 0.7f;
}
